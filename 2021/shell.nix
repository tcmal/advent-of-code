{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
            emacs
            (haskellPackages.ghcWithPackages (p: [
                  p.linear
                  p.parsec
            ]))
            python3
            stack
            racket
            clojure
            leiningen
            (minizinc.overrideAttrs (old: let rev = "adaa07456233d9ffe0a1f848917dde41e8c54710"; in {
                  version = "develop-${rev}";
                  src = pkgs.fetchFromGitHub {
                        owner = "MiniZinc";
                        repo = "libminizinc";
                        
                        rev = rev;
                        sha256 = "sha256-t5/reUj38cc3H7CE1iPWgYD9m+190E5ihFHhft8+Bns=";
                  };
            }))
            (gecode.overrideAttrs (old: let rev = "fec7e9fd99bca98f146416ba8ea8adc278f5a95a"; in {
                  version = "develop-${rev}";
                  src = pkgs.fetchFromGitHub {
                        owner = "Gecode";
                        repo = "gecode";
                        sha256 = "sha256-HiYO74RnxY6ga7uppjR3DXMFOgE/8Gs0dvi86qUQcjo=";
                        rev = rev;
                  };
            }))
	];
}
