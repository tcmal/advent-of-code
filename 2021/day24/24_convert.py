# Converts input to a .dzn file
# Usage: python 24_convert.py | minizinc 24a.mzn -

cmds = open("./input").read().strip().split("\n")

def convert_ops(cmds):
    for c in cmds:
        yield "i_" + c.split(" ")[0]

def convert_arg(a):
    cons = "R" if a in {"x", "y", "z", "w"} else "Imm"
    return "%s(%s)" % (cons, a)

def convert_args(cmds):
    for c in cmds:
        spl = c.split(" ")[1:]
        if len(spl) == 1:
            yield (convert_arg(spl[0]), "None")
        else:
            yield (convert_arg(spl[0]), convert_arg(spl[1]))


inp_ops = list(convert_ops(cmds))
inp_args = list(convert_args(cmds))

print("inp_ops = [ %s ];" % ", ".join(inp_ops))
print("inp_args = [| %s |];" % "|".join([",".join(x) for x in inp_args]))
