use crate::utils::max_n;

mod utils;

fn main() {
    let input = utils::read_input();

    let mut elf_cal_counts: Vec<usize> = input
        .split("\n\n")
        .map(|xs| {
            xs.lines()
                .map(|x| x.parse::<usize>().unwrap())
                .sum::<usize>()
        })
        .collect();

    let top_3 = max_n(&mut elf_cal_counts, 3);

    println!("Max: {}", top_3.last().unwrap());
    println!("Sum of top 3: {}", top_3.iter().sum::<usize>());
}
