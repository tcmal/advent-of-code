{
  description = "AOC 2022";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, crane, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ (import rust-overlay) ];
        };

        toolchain = pkgs.rust-bin.nightly.latest.default.override {
          targets = ["wasm32-unknown-unknown"];
        };
        craneLib = (crane.mkLib pkgs).overrideToolchain toolchain;
        buildInputs = [
            pkgs.cmake
            pkgs.pkg-config
            pkgs.fontconfig
            pkgs.trunk
        ];
        my-crate = craneLib.buildPackage {
          src = craneLib.cleanCargoSource ./.;
          doCheck = false;

          buildInputs = buildInputs ++ pkgs.lib.optionals pkgs.stdenv.isDarwin [
            # Additional darwin specific inputs can be set here
            pkgs.libiconv
          ];
        };
      in
      {
        checks = {
          inherit my-crate;
        };

        packages.default = my-crate;

        apps.default = flake-utils.lib.mkApp {
          drv = my-crate;
        };

        devShells.default = pkgs.mkShell {
          inputsFrom = builtins.attrValues self.checks;

          nativeBuildInputs = buildInputs ++ (with pkgs; [
            (toolchain.override {
              extensions = ["rust-analyzer" "rust-src" "miri"];
            })
          ]);
        };
      });
}
